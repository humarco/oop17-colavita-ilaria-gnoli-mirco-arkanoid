package view;

import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Classe che modella la scena per la visualizzazione dei creatori del gioco. Estende {@link AbstractMenuView}.
 */
public final class CreditsView extends AbstractMenuView {

    private static final Font FONT = Font.font("Jokerman", 30);

    /**
     * @param stage - Finestra dove verrà rappresentata la scena.
     */
    public CreditsView(final Stage stage) {
        super(stage);
    }

    /**
     * 
     */
    @Override
    public Node centerPane() {
        final VBox p = new VBox();

        final Label l = new Label("Gnoli Mirco\nColavita Ilaria");
        l.setFont(FONT);
        l.setTextFill(Color.RED);

        final RotateTransition rot = new RotateTransition();
        rot.setDuration(Duration.seconds(4));
        rot.setToAngle(360);
        rot.setNode(l);

        final TranslateTransition trs = new TranslateTransition();
        trs.setDuration(Duration.seconds(4));

        trs.setToX(getStage().getWidth() / 2);
        trs.setToY(getStage().getWidth() / 2);
        trs.setNode(l);

        p.getChildren().add(l);

        rot.play();
        trs.play();
        return p;
    }

    @Override
    public String getTitle() {
        return "Credits";
    }
}
