package view;

import controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * View che propone di salvare i punteggi alla fine della partita.
 */
public class EndGameView extends Stage {

    /**
     * @param controller - controller dal quale prendere i dati
     */
    public EndGameView(final Controller controller) {
        super();
        this.setTitle("Salvataggio");
        this.initModality(Modality.WINDOW_MODAL); //non posso tornare alla finestra precedente(quella del gioco) se non ho chiuso questa
        this.initOwner(controller.getView()); //"lega" questo stage alla view che la genera (gameview)

        final Label scoreLabel = new Label("Punteggio:");
        final Label scorePoint = new Label(Integer.toString(controller.getGame().getScore()));
        final Label nameLabel = new Label("Inserisci il tuo nome:");
        final TextField nameField = new TextField();

        final Button save = new Button("Salva");
        save.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(final ActionEvent event) {
                if (!nameField.getText().isEmpty()) {
                    close();
                    controller.saveScore(nameField.getText());
                } else {
                    final Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Attenzione");
                    alert.setHeaderText(null);
                    alert.setContentText("Inserire un nome valido.");
                    alert.show();
                    alert.setX(getX() + (getWidth() - alert.getWidth()) / 2);
                    alert.setY(getY() + (getHeight() - alert.getHeight()) / 2);
                }
            }
        });

        final Button notSave = new Button("Non salvare");
        notSave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent event) {
                controller.openMenu();
            }
        });

        final VBox vBox = new VBox();
        vBox.getChildren().addAll(scoreLabel, scorePoint, nameLabel, nameField);
        vBox.setAlignment(Pos.TOP_CENTER);

        final HBox hBox = new HBox();
        hBox.getChildren().addAll(save, notSave);

        final GridPane pane = new GridPane();
        pane.setPadding(new Insets(10));

        pane.add(vBox, 0, 0);
        pane.add(hBox, 0, 1);

        final Scene scene = new Scene(pane);
        this.setScene(scene);

        this.setResizable(false);
        this.sizeToScene();

        this.show();
        this.setX(controller.getView().getX() + ((controller.getView().getWidth() - this.getWidth()) / 2));
        this.setY(controller.getView().getY() + ((controller.getView().getHeight() - this.getHeight()) / 2));
    }
}
