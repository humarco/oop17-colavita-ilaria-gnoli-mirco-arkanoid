package model.entities;

import javafx.util.Pair;
import model.ModelCostant;
/**
 * Bar model. Implements {@link IBar}.
 * 
 */
public class Bar implements IBar {

    /**
     * Number of pixel for key pression.
     */
    private static final int MOVEMENT_DELTA = 8;
    /**
     * Number of pixel for increment and decrement the bar width.
     */
    private static final int MODIFICATION_DELTA = 40;
    /**
     * Minimum bar width.
     */
    private static final int MIN_WIDTH = 40;
    /**
     * Maximum bar width.
     */
    private static final int MAX_WIDTH = 200;

    private int x;
    private int y;
    private int width;
    private final int height;
    private double arcWidth;
    private double arcHeight;

    /**
     * Constructor.
     * 
     * @param x
     *          the bar x position
     * @param y
     *          the bar y position
     * @param width
     *          the bar width
     * @param height
     *          the bar height
     */
    Bar(final int x, final int y, final int width, final int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Constructor.
     * 
     * @param x
     *          the bar x position
     * @param y
     *          the bar y position
     * @param width
     *          the bar width
     * @param height
     *          the bar height
     * @param height
     *          the arc width
     * @param height
     *          the arc height
     */
    Bar(final int x, final int y, final int width, final int height, final double arcWidth, final double arcHeight) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.arcHeight = arcHeight;
        this.arcWidth = arcWidth;
    }

    @Override
    public final int getMinX() {
        return this.x;
    }

    @Override
    public final int getMaxX() {
        return this.x + this.width;
    }

    @Override
    public final int getMinY() {
        return this.y;
    }

    @Override
    public final int getMaxY() {
        return this.y + height;
    }

    @Override
    public final void setPosition(final int newX, final int newY) {
        this.x = newX;
        this.y = newY;
    }

    @Override
    public final Pair<Integer, Integer> getPosition() {
        return new Pair<>(this.x, this.y);
    }
    /**
     * @return the bar length
     */
    public int getLength() {
        return this.width;
    }
    /**
     * @return the bar arc width
     */
    public double getArcWidth() {
        return this.arcWidth;
    }
    /**
     * @return the bar arc height
     */
    public double getArcHeight() {
        return this.arcHeight;
    }

    @Override
    public final void moveRight() {
        if (this.x <= ModelCostant.WORLD_WIDTH - this.width - ModelCostant.DEFAULT_OFFSET_FROM_WALL) {
            this.setPosition(this.x + MOVEMENT_DELTA, this.y);
        }
    }

    @Override
    public final void moveLeft() {
        if (this.x > 0 + ModelCostant.DEFAULT_OFFSET_FROM_WALL) {
            this.setPosition(this.x - MOVEMENT_DELTA, this.y);
        }
    }

    @Override
    public final void extendBar() {
        if (this.width <= MAX_WIDTH) {
            this.width = this.width + MODIFICATION_DELTA;
            this.setPosition(this.x - MODIFICATION_DELTA / 2, this.y);
        }
    }

    @Override
    public final void reduceBar() {
        if (this.width > MIN_WIDTH) {
            this.width = this.width - MODIFICATION_DELTA;
            this.setPosition(this.x + MODIFICATION_DELTA / 2, this.y);
        }
    }

}
