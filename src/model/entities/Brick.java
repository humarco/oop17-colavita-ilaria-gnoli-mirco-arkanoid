package model.entities;

import javafx.scene.paint.Color;
import javafx.util.Pair;

/**
 * Brick model. Implements {@link Entity}.
 * 
 */
public class Brick implements Entity {

    private int x;
    private int y;
    private final int length;
    private final int height;
    private BrickType type;
    private int counter;

    /**
     * Constructor.
     * 
     * @param width
     *          the brick width
     * @param height
     *          the brick height
     */
    public Brick(final int width, final int height) {
        this.length = width;
        this.height = height;
    }

    /**
     * Constructor.
     * 
     * @param x
     *          the brick x position
     * @param y
     *          the brick y position
     * @param width
     *          the brick width
     * @param height
     *          the brick height
     */
    public Brick(final int x, final int y, final int width, final int height) {
        this.x = x;
        this.y = y;
        this.length = width;
        this.height = height;
        this.counter = this.type.getCounter();
    }

    /**
     * @return the brick length
     * 
     */
    public int getWidth() {
        return this.length;
    }

    /**
     * @return the brick height
     * 
     */
    public int getHeight() {
        return this.height;
    }

    @Override
    public final int getMinX() {
        return this.x;
    }

    @Override
    public final int getMaxX() {
        return this.x + length;
    }

    @Override
    public final int getMinY() {
        return this.y;
    }

    @Override
    public final int getMaxY() {
        return this.y + height;
    }

    @Override
    public final Pair<Integer, Integer> getPosition() {
        return new Pair<>(this.x, this.y);
    }

    @Override
    public final void setPosition(final int newX, final int newY) {
        this.x = newX;
        this.y = newY;
    }

    /**
     * Set the brick type.
     * @param type 
     *          type of the Brick{@link @BrickType}
     */
    public final void setBrickType(final BrickType type) {
        this.type = type;
        this.counter = this.type.getCounter();
    }

    /**
     * Get the brick type.
     * 
     * @return type the type of this Brick{@link @BrickType}
     */
    public final BrickType getBrickType() {
        return this.type;
    }

    /**
     * Get the brick color.
     * 
     * @return the color of this Brick
     */
    public final Color getBrickColor() {
        return this.type.getColor();
    }

    /**
     * Get the brick counter. The counter indicate how many hits the brick needs to get destroyed
     * 
     * @return the brick counter
     * 
     */
    public final int getCounter() {
        return this.counter;
    }

    /**
     * Decrement the brick counter until the ball is allowed to destroy the brick.
     * 
     * @return the brick counter - 1
     */
    public final int decCounter() {
        return this.counter--;
    }

    /**
     * Get the points to add at brick break.
     * 
     * @return the points
     * 
     */
    public final int getPoints() {
        return this.type.getPoints();
    }
}
