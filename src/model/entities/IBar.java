package model.entities;

/**
 * This interface represents a Bar. Extends {@link Entity}.
 */
public interface IBar extends Entity {

    /**
     * Moves the bar right of MOVEMENT_DELTA unit.
     */
    void moveRight();

    /**
     * Moves the bar left of MOVEMENT_DELTA unit.
     */
    void moveLeft();

    /**
     * Extends the bar of MODIFY_DELTA unit.
     */
    void extendBar();

    /**
     * Reduces the bar of MODIFY_DELTA unit.
     */
    void reduceBar();

}
